package com.aimei.www;

import com.aimei.security.annotation.SecurityAnnoTation;
import com.aimei.core.moduel.annotation.ParentConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@ParentConfig
@SecurityAnnoTation
public class AuthenticationServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationServerApplication.class, args);
	}
}
