package com.aimei.www.moduel.service.impl;


import com.aimei.core.moduel.entity.User;
import com.aimei.www.moduel.mapper.UserMapper;
import com.aimei.www.moduel.service.UserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author xiah
 * date: 2018/4/26
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
       @Autowired
       private UserMapper userMapperer;

       /*@Override
       public User getUser() {
              *//*使用mybatis-plus分页*//*
              EntityWrapper wrapper = new EntityWrapper();
              wrapper.where("id>{0}", 0);
              *//*从第一条记录开始   不包括第一条   最多返回三条记录*//*
              RowBounds rowBounds = new RowBounds(1, 3);
              List list = userMapperer.selectPage(rowBounds, wrapper);

               *//*使用自定义分页 当前页 每页多少条*//*
              Page page = new Page(1, 3);
              List list2 = userMapperer.getList(page);
              User user = userMapperer.selectById(1L);
              return user;
       }*/

       @Override
       public User getUser() {
              userMapperer.getClass();
              return null;
       }
}
