package com.aimei.www.moduel.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* @author xiah
* date: 2018/4/27
*/ 

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_user")
public class User  {
       private static final long serialVersionUID = 1L;
       @TableId(type = IdType.AUTO)
       private Long id;
       @TableField(value = "user_name")
       private String userName;
       @TableField(value = "cell_phone")
       private String cellPhone;

       private String password;


}
