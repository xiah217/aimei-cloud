package com.aimei.www.moduel.service;


import com.aimei.core.moduel.entity.User;
import com.baomidou.mybatisplus.service.IService;

public interface UserService extends IService<User> {
        User getUser();
}
