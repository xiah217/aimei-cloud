package com.aimei.www.moduel.util;

import com.netflix.zuul.context.RequestContext;

/**
 * @author xiah
 * @date 2018/5/7 14:22
 */
public class RequestContextUtil {
       private RequestContext requestContext;
       private Integer code;
       private String message;

       public RequestContextUtil(RequestContext requestContext,Integer code, String message) {
              requestContext.setSendZuulResponse(false);
              requestContext.setResponseStatusCode(code);
              requestContext.setResponseBody("{\"result\":\"+message+!\"}");
              requestContext.getResponse().setContentType("text/html;charset=UTF-8");
       }
}
